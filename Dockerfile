FROM shincoder/homestead
RUN apt-get update
RUN apt-get install -y php-xdebug
RUN phpenmod xdebug
COPY / /app
RUN ./serve.sh localhost /app/public